package pl.finmatik.sda.todos_demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import pl.finmatik.sda.todos_demo.model.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
    boolean existsByName(String name);

    UserRole findByName(String role);
}
