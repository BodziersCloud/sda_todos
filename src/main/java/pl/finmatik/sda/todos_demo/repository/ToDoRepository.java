package pl.finmatik.sda.todos_demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.finmatik.sda.todos_demo.model.ToDoCard;

import java.util.List;
import java.util.Optional;

public interface ToDoRepository extends JpaRepository<ToDoCard, Long> {
    boolean existsById(Long id);
    Optional<ToDoCard> findById(Long id);

}
