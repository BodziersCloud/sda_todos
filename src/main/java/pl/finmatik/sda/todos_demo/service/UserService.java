package pl.finmatik.sda.todos_demo.service;

import pl.finmatik.sda.todos_demo.model.AppUser;
import pl.finmatik.sda.todos_demo.model.ToDoCard;

import java.util.List;

public interface UserService {
    void registerUser(String username, String password, String passwordConfirm);
    List<AppUser> getAllUsers();

}
