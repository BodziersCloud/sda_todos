package pl.finmatik.sda.todos_demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.finmatik.sda.todos_demo.model.ToDoCard;
import pl.finmatik.sda.todos_demo.repository.AppUserRepository;
import pl.finmatik.sda.todos_demo.repository.ToDoRepository;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class ToDoServiceImpl implements ToDoService {
    private ToDoRepository toDoRepository;
    private AppUserRepository appUserRepository;

    @Autowired
    public ToDoServiceImpl(ToDoRepository toDoRepository, AppUserRepository appUserRepository) {
        this.toDoRepository = toDoRepository;
        this.appUserRepository = appUserRepository;
    }


    @Override
    public void addToDo(String taskTitle, String taskDescription, LocalDate creationDate, LocalDate dueDate, Boolean isDone) {

        ToDoCard toDoCard = new ToDoCard();
        toDoCard.setCreationDate(LocalDate.now());
        toDoCard.setDueDate(dueDate);
        toDoCard.setTaskTitle(taskTitle);
        toDoCard.setTaskDescription(taskDescription);
        toDoCard.setIsDone(isDone);

        toDoRepository.save(toDoCard);

        //get logged user id from current session

        //write user id to current todos
    }

    @Override
    public List<ToDoCard> getAllToDos() {
        return toDoRepository.findAll();
    }

    @Override
    public Optional<ToDoCard> findById(Long id) {
        return toDoRepository.findById(id);
    }

    @Override
    public void delToDoById(Long id) {
        toDoRepository.deleteById(id);
    }
}
