package pl.finmatik.sda.todos_demo.service;


import pl.finmatik.sda.todos_demo.model.UserRole;

import java.util.Set;

public interface UserRoleService {
    Set<UserRole> getDefaultUserRoles();
}
