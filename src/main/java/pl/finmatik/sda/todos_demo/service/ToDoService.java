package pl.finmatik.sda.todos_demo.service;

import pl.finmatik.sda.todos_demo.model.ToDoCard;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ToDoService {

    void addToDo(String taskTitle, String taskDescription, LocalDate creationDate, LocalDate dueDate, Boolean isDone);

    List<ToDoCard> getAllToDos();

    Optional<ToDoCard> findById(Long id);

    void delToDoById(Long id);


}
