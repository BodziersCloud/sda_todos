package pl.finmatik.sda.todos_demo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.finmatik.sda.todos_demo.service.ToDoService;
import pl.finmatik.sda.todos_demo.service.UserService;

import java.time.LocalDate;

@Controller
@RequestMapping("/admin/")
public class AdminController {

    @Autowired
    private ToDoService toDoService;

    @Autowired
    private UserService userService;

    @GetMapping("/user/list")
    public String getUserList(Model model) {
        model.addAttribute("userList", userService.getAllUsers());
        return "userList";
    }

    @GetMapping("/task/list")
    public String getAddToDoForm(Model model) {
        model.addAttribute("toDoList",toDoService.getAllToDos());
        return "todoForm";
    }

    @PostMapping("/task/list")
    public String submitToDoForm(
            @RequestParam(name = "todo_title") String taskTitle,
            @RequestParam(name = "task_description") String taskDescription,
            @RequestParam(name = "creation_date")  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate creationDate,
            @RequestParam(name = "due_date")  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dueDate,
            @RequestParam(name = "is_done", defaultValue="false") boolean isDone) {
        toDoService.addToDo(taskTitle, taskDescription, creationDate, dueDate, isDone);
        return "redirect:/todo/savetodo";
    }

}
