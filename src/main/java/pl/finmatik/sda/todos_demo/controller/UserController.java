package pl.finmatik.sda.todos_demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.finmatik.sda.todos_demo.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

//    @GetMapping("/register")
//    public String getRegisterForm() {
//        return "userRegister"; // załaduj widok userRegister (formularz)
//    }
//
//    @PostMapping("/register")
//    public String submitRegisterForm(@RequestParam(name = "username") String username,
//                                     @RequestParam(name = "password") String password,
//                                     @RequestParam(name = "password-confirm") String passwordConfirm){
//        userService.registerUser(username, password, passwordConfirm); // zapisz użytkownika w bazie
//
//        return "redirect:/login"; // przekieruj
//    }

    @GetMapping("/register2")
    public String getRegisterForm2() {
        return "register"; // załaduj widok userRegister (formularz)
    }

    @PostMapping("/register2")
    public String submitRegisterForm2(@RequestParam(name = "username") String username,
                                     @RequestParam(name = "password") String password,
                                     @RequestParam(name = "password-confirm") String passwordConfirm){
        userService.registerUser(username, password, passwordConfirm); // zapisz użytkownika w bazie

        return "redirect:/login2"; // przekieruj
    }
}
