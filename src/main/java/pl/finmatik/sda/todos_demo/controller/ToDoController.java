package pl.finmatik.sda.todos_demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import pl.finmatik.sda.todos_demo.model.ToDoCard;
import pl.finmatik.sda.todos_demo.repository.ToDoRepository;
import pl.finmatik.sda.todos_demo.service.ToDoService;

import java.time.LocalDate;
import java.util.Optional;

import static org.hibernate.cfg.AvailableSettings.USER;


@Controller
@RequestMapping("/task")
public class ToDoController {
    @Autowired
    private ToDoService toDoService;


    @GetMapping("/add")
    public String getAddToDoForm(Model model) {
        model.addAttribute("toDoList",toDoService.getAllToDos());
        return "todoForm";
    }

    @PostMapping("/add")
    public String submitToDoForm(
            @RequestParam(name = "todo_title") String taskTitle,
            @RequestParam(name = "task_description") String taskDescription,
            @RequestParam(name = "creation_date")  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate creationDate,
            @RequestParam(name = "due_date")  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dueDate,
            @RequestParam(name = "is_done", defaultValue="false") boolean isDone) {


        toDoService.addToDo(taskTitle, taskDescription, creationDate, dueDate, isDone);

        return "redirect:/task/add";
    }

    @GetMapping(path = "/details")
    public String toDoDetails(@RequestParam(name = "toDoId") Long id, Model model) {
        Optional<ToDoCard> toDoCardOptionalptional = toDoService.findById(id);
        if (toDoCardOptionalptional.isPresent()) {
            // mamy to do, obsługa i wyświetlenie
            model.addAttribute("todo", toDoCardOptionalptional.get());
            return "todoDetails";
        }
        // nie ma mieszkania, cóż zrobić?
        return "redirect:/task/add";
    }
    @PostMapping("/details")
    public String submitDetailsToDoForm(

            @RequestParam(name = "taskTitle") String taskTitle,
            @RequestParam(name = "taskDescription") String taskDescription,
            @RequestParam(name = "creation_date")  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate creationDate,
            @RequestParam(name = "due_date")  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dueDate,
            @RequestParam(name = "isDone", defaultValue="false") boolean isDone) {
        toDoService.addToDo(taskTitle, taskDescription, creationDate, dueDate, isDone);
        return "redirect:/task/add";
    }


    @GetMapping(path = "/delete")
    public String deleteToDo(@RequestParam(name = "toDoId") Long id) {
        toDoService.delToDoById(id);
        return "redirect:/task/add";
    }



}
