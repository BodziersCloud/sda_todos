package pl.finmatik.sda.todos_demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/")
public class IndexController {

    @GetMapping(path = "/")
    public String index() {
        return "index";
    }

    @GetMapping("/login")
    public String loginForm() {
        return "loginForm";
    }
    @GetMapping("/login2")
    public String loginForm2() {
        return "login";
    }

    @GetMapping("/logout*")
    public String logOut() {
        return "redirect: login";
    }
}
