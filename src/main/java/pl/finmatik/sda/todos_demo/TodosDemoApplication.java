package pl.finmatik.sda.todos_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodosDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(TodosDemoApplication.class, args);
    }

}
